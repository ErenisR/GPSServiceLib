package gpslibrary.erenis.com.gpslibrary;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by ereni on 11-Oct-17.
 */

public class SettingsFragment extends PreferenceFragment
{
    private static final String TAG = SettingsFragment.class.getSimpleName();

    private Activity mActivity;

    private AlarmManager alarmManager;

    private TimeDialog timeDialog;

    private PendingIntent pendingIntent;

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fragmented_preferences_inner);

        timeDialog = (TimeDialog) getPreferenceScreen().findPreference(getString(R.string.time_preference));
        timeDialog.registerListener(mTimeDialogListener);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(mOnPreferenceChanged);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(mOnPreferenceChanged);
    }


    private void setAlarmManager()
    {
        if (alarmManager == null)
        {
            alarmManager = (AlarmManager) mActivity.getSystemService(Context.ALARM_SERVICE);
        }
    }


    private SharedPreferences.OnSharedPreferenceChangeListener mOnPreferenceChanged = new SharedPreferences.OnSharedPreferenceChangeListener()
    {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s)
        {
            if (s.equals(getString(R.string.switch_location_preference)))
            {
                boolean b = sharedPreferences.getBoolean(s, false);
                Log.d(TAG, "Switch preference: " + b);
                if (b)
                {
                    String time = sharedPreferences.getString(getString(R.string.time_preference), "12:00");
                    int h = Integer.valueOf(time.substring(0, time.indexOf(":")));
                    int m = Integer.valueOf(time.substring(time.indexOf(":") + 1));

                    setAlarm(h, m);
                }
                else
                {
                    cancelAlarm();
                }
            }
        }
    };

    private TimeDialog.TimeDialogListener mTimeDialogListener = new TimeDialog.TimeDialogListener()
    {
        @Override
        public void onTimeChanged(int hour, int minutes)
        {
            setAlarm(hour, minutes);
        }
    };

    private void setAlarm(int hour, int minutes)
    {
        setAlarmManager();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);

        pendingIntent = PendingIntent.getBroadcast(mActivity, 0, new Intent(mActivity, SampleAlarmReceiver.class), 0);

        long timeMillis = calendar.getTimeInMillis();
        Log.d(TAG, "Time: " + timeMillis);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeMillis, AlarmManager.INTERVAL_DAY, pendingIntent);

        Log.d(TAG, "Time set.");
    }

    private void cancelAlarm()
    {
        if (alarmManager != null)
        {
            alarmManager.cancel(pendingIntent);
        }
    }
}