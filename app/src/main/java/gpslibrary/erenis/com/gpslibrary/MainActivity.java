package gpslibrary.erenis.com.gpslibrary;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

import gpslibrary.erenis.com.gpslib.GPSLocationChangedListener;
import gpslibrary.erenis.com.gpslib.GPSService;

public class MainActivity extends AppCompatActivity implements GPSLocationChangedListener
{

    private static String TAG = MainActivity.class.getSimpleName();

    private Button mButton;

    private TextView mTextView;

    private TextView mTextView1;

    private Location mLocation;

    private boolean updateLocation;

    private GPSService service;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.widget_toolbar_main);
        setSupportActionBar(toolbar);

        mButton = (Button) findViewById(R.id.widget_button_start_stop_service);
        mTextView = (TextView) findViewById(R.id.widget_text_view_current_speed);
        mTextView1 = (TextView) findViewById(R.id.widget_text_view_current_location);

        mButton.setOnClickListener(mOnClickListener);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        updateLocation = sharedPref.getBoolean(getString(R.string.switch_location_preference), false);

        if (updateLocation)
        {
            service = new GPSService(this, this);
            mLocation = service.getLocation();
        }
        toggleSpeed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    mLocation = service.getLocation();
                    toggleSpeed();
                }
                else
                {
                    onRequestPermission();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings)
        {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleSpeed()
    {
        if (updateLocation)
        {
            if (mLocation != null)
            {
                DecimalFormat df = new DecimalFormat("##.##");
                String currentSpeed = df.format(3.6 * mLocation.getSpeed());
                mTextView.setText("Current Speed: ".concat(currentSpeed).concat("km/h"));

                Log.d(TAG, "toggleSpeed: " + mLocation.getSpeed());

                String latitude = df.format(mLocation.getLatitude());
                String longitude = df.format(mLocation.getLongitude());

                mTextView1.setText("Current coordinates: ".concat(latitude).concat(", ").concat(longitude));
            }
            else
            {
                mTextView.setText(R.string.text_view_speed_0);
            }
            mButton.setText(R.string.button_stop_speed);
        }
        else
        {
            mTextView.setText(R.string.text_view_no_gps);
            mButton.setText(R.string.button_show_speed);
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        if (location != null)
        {
            Log.d(TAG, "Location: " + location.getLatitude() + ", " + location.getLongitude());
            mLocation = location;
            toggleSpeed();
        }
    }

    @Override
    public void onRequestPermission()
    {
        Log.d(TAG, "onRequestPermission");
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int id = view.getId();

            switch (id)
            {
                case R.id.widget_button_start_stop_service:
                    if (!mButton.getText().toString().toLowerCase().contains("stop"))
                    {
                        toggleSpeed();
                        mButton.setText(R.string.button_stop_speed);
                    }
                    else
                    {
                        mButton.setText(R.string.button_show_speed);
                        mTextView.setText(R.string.button_click_thue_button);
                    }
                    break;
            }
        }
    };
}
