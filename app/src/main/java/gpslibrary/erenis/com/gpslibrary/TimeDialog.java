package gpslibrary.erenis.com.gpslibrary;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import java.util.Locale;

/**
 * Created by ereni on 09-Oct-17.
 */

public class TimeDialog extends DialogPreference
{
    public interface TimeDialogListener
    {
        void onTimeChanged(int hour, int minutes);
    }

    private static final String TAG = TimeDialog.class.getSimpleName();

    private static String DEFAULT_VALUE = "9:30";

    private NumberPicker hour;

    private NumberPicker minutes;

    private String current;

    private TimeDialogListener mListener;

    public void registerListener(TimeDialogListener listener)
    {
        mListener = listener;
    }

    public TimeDialog(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        setDialogLayoutResource(R.layout.dialog_setup_schedule);
        setPositiveButtonText(R.string.button_ok);
        setNegativeButtonText(R.string.button_cancel);
    }

    @Override
    protected void onBindDialogView(View view)
    {
        super.onBindDialogView(view);
        initViews(view);
    }

    private void initViews(View view)
    {
        hour = view.findViewById(R.id.widget_number_picker_hour);
        minutes = view.findViewById(R.id.widget_number_picker_minutes);

        hour.setMinValue(0);
        hour.setMaxValue(23);

        minutes.setMinValue(0);
        minutes.setMaxValue(59);

        NumberPicker.Formatter formatter = new NumberPicker.Formatter()
        {
            @Override
            public String format(int i)
            {
                return String.format(Locale.ENGLISH, "%02d", i);
            }
        };
        hour.setFormatter(formatter);
        minutes.setFormatter(formatter);

        setValues();
    }

    private void setValues()
    {
        if (current != null && !current.equals(""))
        {
            int m = Integer.valueOf(current.substring(current.indexOf(":") + 1));
            int h = Integer.valueOf(current.substring(0, current.indexOf(":")));

            hour.setValue(h);
            minutes.setValue(m);
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which)
    {
        switch (which)
        {
            case DialogInterface.BUTTON_POSITIVE:
                Log.d(TAG, "Ok");
                String newValue = hour.getValue() + ":" + minutes.getValue();
                persistString(newValue);
                current = newValue;
                mListener.onTimeChanged(hour.getValue(), minutes.getValue());
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        if (positiveResult)
        {
            String newValue = hour.getValue() + ":" + minutes.getValue();
            persistString(newValue);
        }
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue)
    {
        Log.d(TAG, "OnSetInitialValue");
        if (restorePersistedValue)
        {
            current = getPersistedString(DEFAULT_VALUE);
        }
        else
        {
            current = (String) defaultValue;
            persistString(current);
        }
        Log.d(TAG, "Current: " + current);
    }
}