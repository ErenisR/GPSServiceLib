package gpslibrary.erenis.com.gpslibrary;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import gpslibrary.erenis.com.gpslib.GPSLocationChangedListener;
import gpslibrary.erenis.com.gpslib.GPSService;

public class SampleSchedulingService extends IntentService
{
    public SampleSchedulingService()
    {
        super("SchedulingService");
    }

    public static final String TAG = "Scheduling Demo";

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Log.d(TAG, "onHandleIntent");
        // Release the wake lock provided by the BroadcastReceiver.
        SampleAlarmReceiver.completeWakefulIntent(intent);
    }
}