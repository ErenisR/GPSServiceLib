package gpslibrary.erenis.com.gpslibrary;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by ereni on 04-Oct-17. Settings screen
 */

public class SettingsActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment(), SettingsFragment.class.getName()).commit();
    }
}