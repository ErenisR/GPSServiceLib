package gpslibrary.erenis.com.gpslib;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

/**
 * Created by ereni on 04-Oct-17.
 */

public class GPSService
{
    private static final String TAG = GPSService.class.getSimpleName();

    private Context mContext;

    private GPSLocationChangedListener mListener;

    public GPSService(Context context, GPSLocationChangedListener listener)
    {
        mContext = context;
        mListener = listener;
    }

    public Location getLocation()
    {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null)
        {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                mListener.onRequestPermission();
            }
            else
            {
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5000, mLocationListener);
                if (location != null)
                {
                    if (mListener != null)
                    {
                        mListener.onLocationChanged(location);
                    }
                    return location;
                }
            }
        }
        else
        {
            Toast.makeText(mContext, "This device does not support location services!", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    private LocationListener mLocationListener = new LocationListener()
    {
        @Override
        public void onLocationChanged(Location location)
        {
            if (mListener != null)
            {
                mListener.onLocationChanged(location);
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle)
        {

        }

        @Override
        public void onProviderEnabled(String s)
        {

        }

        @Override
        public void onProviderDisabled(String s)
        {

        }
    };
}
