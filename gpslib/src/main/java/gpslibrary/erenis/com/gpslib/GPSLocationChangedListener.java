package gpslibrary.erenis.com.gpslib;

import android.location.Location;

/**
 * Created by ereni on 04-Oct-17.
 */

public interface GPSLocationChangedListener
{
    void onLocationChanged(Location location);

    void onRequestPermission();
}
